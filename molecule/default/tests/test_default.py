import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_dhcp_is_installed(host):
    assert host.package("dhcp").is_installed


def test_dhcpd_is_running_and_enabled(host):
    assert host.service("dhcpd").is_running
    assert host.service("dhcpd").is_enabled

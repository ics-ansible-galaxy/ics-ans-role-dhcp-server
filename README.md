ics-ans-role-dhcp-server
===================

Configure DHCP Server.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

- ...

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - { role: ics-ans-role-dhcp-server }
```

License
-------

BSD 2-clause
